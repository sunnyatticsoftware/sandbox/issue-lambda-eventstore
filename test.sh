#!/bin/sh
SERVICE_NAME="eventstore-lambda-sample"
HANDLER="EventStoreDb.LambdaSample::EventStoreDb.LambdaSample.LambdaEntryPoint::FunctionHandler"
ROLE_ARN="arn:aws:iam::308309238958:role/sample-lambda-ex"

dotnet publish -c Release -o publish
cd publish
zip -r ../$SERVICE_NAME.zip *
cd ..
aws --profile diegosasw iam create-role --role-name $SERVICE_NAME-ex --assume-role-policy-document '{"Version": "2012-10-17", "Statement": [{ "Effect": "Allow", "Principal": {"Service": "lambda.amazonaws.com"}, "Action": "sts:AssumeRole"}]}'
aws --profile diegosasw iam attach-role-policy --role-name $SERVICE_NAME-ex --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
aws --profile diegosasw lambda delete-function --function-name $SERVICE_NAME-function
aws --profile diegosasw lambda create-function --function-name $SERVICE_NAME-function --zip-file fileb://$SERVICE_NAME.zip --handler $HANDLER --runtime dotnetcore3.1 --role $ROLE_ARN
aws --profile diegosasw lambda update-function-configuration --function-name $SERVICE_NAME-function --timeout 10
aws --profile diegosasw lambda invoke --function-name $SERVICE_NAME-function --payload file://sample_payload.json response.json --log-type Tail