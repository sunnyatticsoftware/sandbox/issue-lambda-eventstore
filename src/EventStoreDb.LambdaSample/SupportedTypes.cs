﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace EventStoreDb.LambdaSample
{
    public class SupportedTypes
    {
        private readonly Dictionary<string, Type> _supportedEventTypes = new Dictionary<string, Type>();

        public IDictionary<string, Type> EventTypes => new ReadOnlyDictionary<string, Type>(_supportedEventTypes);
        
        public SupportedTypes AddType(Type type)
        {
            _supportedEventTypes[type.Name] = type;
            return this;
        }
        
        public SupportedTypes AddType<T>()
            where T : class
        {
            return AddType(typeof(T));
        }

        public static SupportedTypes Default => new SupportedTypes();
    }
}