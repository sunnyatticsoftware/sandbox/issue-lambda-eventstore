﻿namespace Rubiko.EventStore.EventStoreDb.LambdaSample
{
    public class Foo
    {
        public string Message { get; }

        public Foo(string message)
        {
            Message = message;
        }
        
        public override string ToString()
        {
            return $"{this.GetType().Name}, Message={Message}";
        }
    }

    public class Bar
    {
        public Bar(string message)
        {
            Message = message;
        }
        public string Message { get; }
        
        public override string ToString()
        {
            return $"{this.GetType().Name}, Message={Message}";
        }
    }
}