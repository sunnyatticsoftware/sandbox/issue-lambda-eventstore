using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using Rubiko.EventStore.EventStoreDb.LambdaSample;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace EventStoreDb.LambdaSample
{
    public class LambdaEntryPoint
    {
        private const string ConnectionString = "esdb://15.236.204.20:2113?tls=false";
        //private const string ConnectionString = "esdb://15.236.204.20:2113?tls=false";
        private const string StreamNamePrefix = "ConsoleSample";
        
        /// <summary>
        /// Default constructor that Lambda will invoke.
        /// </summary>
        public LambdaEntryPoint()
        {
        }


        /// <summary>
        /// A Lambda function to respond to HTTP methods from API Gateway and persist events in an eventstore
        /// </summary>
        /// <param name="request">the api gateway proxy request</param>
        /// <returns>The API Gateway response.</returns>
        public async Task<APIGatewayProxyResponse> FunctionHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            context.Logger.LogLine("Get Request\n");
            context.Logger.LogLine("Storing sample events in event store");

            var eventStoreClient = EventStoreClientFactory.Create(ConnectionString);
            var serializationService = new SerializationService(SupportedTypes.Default);
            var eventStore = new global::EventStoreDb.LambdaSample.EventStore(eventStoreClient, serializationService);
            var foo = new Foo("First event");
            var bar = new Bar( "Second event");

            var events = new List<object> {foo, bar};

            var id = Guid.NewGuid();
            var streamName = $"{StreamNamePrefix}-{id}";
            
            await eventStore.Append(streamName, events.Count, events, CancellationToken.None);
            
            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = "Events persisted successfully",
                Headers = new Dictionary<string, string> { { "Content-Type", "text/plain" } }
            };

            return response;
        }
    }
}
