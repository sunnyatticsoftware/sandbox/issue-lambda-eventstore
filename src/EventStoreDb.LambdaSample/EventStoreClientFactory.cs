﻿using EventStore.Client;

namespace EventStoreDb.LambdaSample
{
    public static class EventStoreClientFactory
    {
        public static EventStoreClient Create(string connectionString)
        {
            var settings = EventStoreClientSettings.Create(connectionString);
            var client = new EventStoreClient(settings);
            return client;
        }
    }
}