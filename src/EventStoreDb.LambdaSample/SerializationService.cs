﻿using EventStore.Client;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text.Json;

namespace EventStoreDb.LambdaSample
{
    public class SerializationService
    {
        private readonly SupportedTypes _supportedTypes;

        public SerializationService(SupportedTypes supportedTypes)
        {
            _supportedTypes = supportedTypes;
        }
        
        public EventData GetAsEventData(object eventToPersist)
        {
            var id = Uuid.NewUuid();
            var eventTypeName = eventToPersist.GetType().Name;
            var data = JsonSerializer.SerializeToUtf8Bytes(eventToPersist);
            var eventData = new EventData(id, eventTypeName, data);
            return eventData;
        }

        public object GetAsEvent(ResolvedEvent resolvedEvent)
        {
            var eventRecorded = resolvedEvent.Event;
            var eventTypeName = eventRecorded.EventType;
            var isSupported = _supportedTypes.EventTypes.TryGetValue(eventTypeName, out var eventType);
            if (eventType is null)
            {
                throw new KeyNotFoundException($"Could not retrieve type for {eventTypeName}");
            }
            if (!isSupported)
            {
                throw new KeyNotFoundException($"Could not find {eventTypeName} in the collection of supported ones");
            }
            var eventRetrieved = JsonSerializer.Deserialize(eventRecorded.Data.ToArray(), eventType);
            if (eventRetrieved is null)
            {
                throw new SerializationException($"Could not deserialize event recorded into {eventType}");
            }
            return eventRetrieved;
        }
    }
}