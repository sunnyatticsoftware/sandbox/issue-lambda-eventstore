﻿using EventStore.Client;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EventStoreDb.LambdaSample
{
    public class EventStore
    {
        private readonly EventStoreClient _eventStoreClient;
        private readonly SerializationService _serializationService;

        public EventStore(
            EventStoreClient eventStoreClient, 
            SerializationService serializationService)
        {
            _eventStoreClient = eventStoreClient;
            _serializationService = serializationService;
        }
        
        public async Task Append(string streamName, int aggregateVersion, IEnumerable<object> events, CancellationToken cancellationToken)
        {
            var eventsData = events.Select(_serializationService.GetAsEventData).ToArray();
            await _eventStoreClient.AppendToStreamAsync(streamName, StreamState.Any, eventsData, cancellationToken: cancellationToken);
        }

        public async Task<IEnumerable<object>> Read(string streamName, CancellationToken cancellationToken)
        {
            var stream = _eventStoreClient.ReadStreamAsync(Direction.Forwards, streamName,StreamPosition.Start, cancellationToken: cancellationToken);
            var resolvedEvents = await stream.ToListAsync(cancellationToken);
            var events = resolvedEvents.Select(_serializationService.GetAsEvent).ToList();
            return events;
        }
    }
}